package com.blog.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.blog.payloads.UserDto;
import com.blog.services.FileService;
import com.blog.services.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;
	@Autowired
	private FileService fileService;
	
	@Value("${project.image}")
	private String path;

	//POST - create user
	@PostMapping("/")
	public ResponseEntity<UserDto> createUser(
			@Valid @RequestBody UserDto userDto){
		UserDto createUserDto = this.userService.createUser(userDto);
		return new ResponseEntity<UserDto>(createUserDto,HttpStatus.CREATED);
	}

	//PUT - update user
	@PutMapping("/{uid}")
	public ResponseEntity<UserDto> updateUser(
			@Valid @RequestBody UserDto userDto, 
			@PathVariable Integer uid){
		UserDto updatedUserDto = this.userService.updateUser(userDto, uid);
		return ResponseEntity.ok(updatedUserDto);
	}

	//ADMIN
	//DELETE - delete user
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	@SuppressWarnings("unchecked")
	@DeleteMapping("/{uid}")
	public ResponseEntity<?> deleteUser(@PathVariable Integer uid){
		this.userService.deleteUser(uid);
		return new ResponseEntity(Map.of("message", "User deleted succesfully."), HttpStatus.OK);
	}
	
	//GET - get all users
//	@PreAuthorize("hasAuthority('ADMIN_USER')")
	@GetMapping("/")
	public ResponseEntity<List<UserDto>> getAllUsers(){
		return ResponseEntity.ok(this.userService.getAllUsers());
	}
	
	//GET - get user by userId
//	@PreAuthorize("hasAuthority('ADMIN_USER')")
	@GetMapping("/{uid}")
	public ResponseEntity<UserDto> getUser(@PathVariable Integer uid) {
		return ResponseEntity.ok(this.userService.getUserById(uid));
	}
	
	//POST - set profile picture for user
		@PostMapping("/{uid}/updateImage")
		public ResponseEntity<UserDto> setProfilePicture(
				@RequestParam MultipartFile image,
				@PathVariable Integer uid) throws IOException {
			String fileName = this.fileService.uploadImage(path, image);
			
			UserDto user = this.userService.getUserById(uid);
			user.setAvatar(fileName);
			UserDto updatedUser = this.userService.updateUser(user, uid);
			
			return new ResponseEntity<UserDto>(updatedUser,HttpStatus.OK);
		}
		
		//GET - get image of user
		@GetMapping(value = "/{uid}/getImage",produces = {MediaType.IMAGE_JPEG_VALUE,MediaType.IMAGE_PNG_VALUE})
		public void getUserImage(
				@PathVariable Integer uid,
				HttpServletResponse response) throws IOException {
			
			UserDto user = this.userService.getUserById(uid);
			String avatar = user.getAvatar();
			InputStream resource = this.fileService.getResource(path, avatar);
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			StreamUtils.copy(resource, response.getOutputStream());
		}
	
}
