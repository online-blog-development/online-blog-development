package com.blog.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.blog.payloads.ApiResponse;
import com.blog.payloads.PostDto;
import com.blog.payloads.PostResponse;
import com.blog.pojos.Post;
import com.blog.services.PostService;
import com.blog.services.FileService;


@RestController
@RequestMapping("/")
//@CrossOrigin(origins = "http://localhost:3000")
public class PostController {

	@Autowired
	PostService postService;
	
	@Autowired
	FileService fileService;
	@Value("${project.image}")
	private String path;

	//POST - Create Post
	@PostMapping("/user/{userId}/category/{categoryId}/posts")
	public ResponseEntity<PostDto> createPost(
			@RequestBody PostDto postDto,
			@PathVariable Integer userId,
			@PathVariable Integer categoryId){
		PostDto createPost = this.postService.createPost(postDto, userId, categoryId);
		return new ResponseEntity<PostDto>(createPost, HttpStatus.CREATED);
	}

	//PUT - update posts
	@PutMapping("/posts/{postId}")
	public ResponseEntity<PostDto> updatePost(@RequestBody PostDto postDto,@PathVariable Integer postId) {
		PostDto updatedPost = this.postService.updatePost(postDto, postId); 
		return new ResponseEntity<PostDto>(updatedPost, HttpStatus.OK);
	}

	//DELETE - Delete Post
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	@DeleteMapping("/posts/{postId}")
	public ApiResponse deletePost(@PathVariable Integer postId) {
		this.postService.deletePost(postId);
		return new ApiResponse("Post is successfully deleted !!", true);
	}

	//GET - Get all posts
	@GetMapping("/posts")
	public ResponseEntity<PostResponse> getAllPost(
			@RequestParam (value = "pageNumber", defaultValue = "0", required = false)Integer pageNumber, 
			@RequestParam(value = "pageSize", defaultValue = "5", required = false)Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "addedDate", required = false)String sortBy){
		PostResponse allPost = this.postService.getAllPosts( pageNumber, pageSize, sortBy);
		return new ResponseEntity<PostResponse>(allPost, HttpStatus.OK);
	}
	
	@GetMapping("/post")
	public ResponseEntity<List<PostDto>> getAllPosts(){
		List<PostDto> allPost = this.postService.getAllPosts();
		return new ResponseEntity<List<PostDto>>(allPost, HttpStatus.OK);
	}

	//GET - Get posts by postId
	@GetMapping("/posts/{postId}")
	public ResponseEntity<PostDto> getPostById(@PathVariable Integer postId){
		PostDto postDto = this.postService.getPostById(postId);
		return new ResponseEntity<PostDto>(postDto, HttpStatus.OK);
	}
	
	//Get - Get posts by UserId
	@GetMapping("/user/{userId}/posts")
	public ResponseEntity<PostResponse> getPostsByUserId(@PathVariable Integer userId,
			@RequestParam (value = "pageNumber", defaultValue = "0", required = false)Integer pageNumber, 
			@RequestParam(value = "pageSize", defaultValue = "5", required = false)Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "addedDate", required = false)String sortBy){
		PostResponse posts = this.postService.getPostByUserId(userId, pageNumber, pageSize, sortBy);
		return new ResponseEntity<PostResponse> (posts, HttpStatus.OK);
	}

	//GET - Get posts by CategoryId
	@GetMapping("/category/{categoryId}/posts")
	public ResponseEntity<PostResponse> getPostsByCategoryId(@PathVariable Integer categoryId,
			@RequestParam (value = "pageNumber", defaultValue = "0", required = false)Integer pageNumber, 
			@RequestParam(value = "pageSize", defaultValue = "5", required = false)Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "addedDate", required = false)String sortBy){
		PostResponse posts = this.postService.getPostByCategoryId(categoryId, pageNumber, pageSize, sortBy);
		return new ResponseEntity<PostResponse> (posts, HttpStatus.OK);
	}

	//Search Post
	@GetMapping("/post/search/{keywords}")
	public ResponseEntity<List<PostDto>> searchPostByTitle(
					@PathVariable ("keywords") String keywords){
		List<PostDto> results = this.postService.searchPost(keywords);
		return new ResponseEntity<List<PostDto>>(results, HttpStatus.OK);
	}
	
	//POST - image
	@PostMapping("/post/image/upload/{postId}")
	public ResponseEntity<PostDto> uploadPostImage(@RequestParam("image") MultipartFile image,
			@PathVariable Integer postId) throws IOException{
		String fileName = this.fileService.uploadImage(path, image);
		
		PostDto postDto = this.postService.getPostById(postId);
		postDto.setImageName(fileName);
		PostDto updatedPost = this.postService.updatePost(postDto, postId);
		return new ResponseEntity<PostDto>(updatedPost,HttpStatus.OK);
	}
	
	//GET - image
	@GetMapping(value="/post/{postId}/image",produces = {MediaType.IMAGE_JPEG_VALUE,MediaType.IMAGE_PNG_VALUE})
	public void downloadImage(
			@PathVariable Integer postId,
			HttpServletResponse response) throws IOException {
		
		PostDto postDto = this.postService.getPostById(postId);
		String imageName = postDto.getImageName();
		InputStream resource = fileService.getResource(path, imageName);
		
		response.setContentType(MediaType.IMAGE_JPEG_VALUE);
		StreamUtils.copy(resource, response.getOutputStream());
	}
	






}
