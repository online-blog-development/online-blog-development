package com.blog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blog.payloads.ApiResponse;
import com.blog.payloads.CommentsDto;
import com.blog.services.CommentsService;

@RestController
@RequestMapping("/comments")

public class CommentsController {
	
	@Autowired
	private CommentsService commentService;

	//POST - create comments
	@PostMapping("/users/{uid}/post/{postId}/comments")
	public ResponseEntity<CommentsDto> createComment(
			@RequestBody CommentsDto comment, 
			@PathVariable Integer uid, 
			@PathVariable Integer postId)
	{				
		CommentsDto createcomment=this.commentService.createComment(comment, postId, uid);
		return new ResponseEntity<CommentsDto>(createcomment,HttpStatus.CREATED);
	}
	
	//DELETE - delete comments
	@DeleteMapping("/comments/{commentId}")
	public ResponseEntity<ApiResponse> deleteComment(@PathVariable Integer commentId)
	{				
		this.commentService.deleteComment(commentId);
		
		return new ResponseEntity<ApiResponse>(new ApiResponse("Comment deleted Successfully...!!!",true),HttpStatus.OK);
	}
}
