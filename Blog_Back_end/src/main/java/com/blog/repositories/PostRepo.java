package com.blog.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.blog.pojos.Category;
import com.blog.pojos.Post;
import com.blog.pojos.User;

public interface PostRepo extends JpaRepository<Post , Integer>{
	
	Page<Post> findByUser(User user, Pageable p);
	
	Page<Post> findByCategory(Category category, Pageable p);

	List<Post> findByTitleContaining(String keywords);
	
	List<Post> findByCategory(Category category);

}
