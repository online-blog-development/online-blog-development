package com.blog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blog.pojos.Comments;

public interface CommentsRepo extends JpaRepository<Comments, Integer> {

	
}
