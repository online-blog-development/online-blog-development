package com.blog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.blog.pojos.Category;

public interface CategoryRepo extends JpaRepository<Category, Integer>{

	@Query("Select c FROM Category c WHERE c.categoryTitle = 'Others'")
	Category findCategoryOther();
}
