package com.blog.services;

import java.util.List;

import com.blog.payloads.PostDto;
import com.blog.payloads.PostResponse;
import com.blog.pojos.Post;


public interface PostService {

	PostDto createPost(PostDto postDto, Integer userId, Integer categoryId);

	PostDto updatePost(PostDto postDto, Integer postId);

	void deletePost(Integer postId);
	
	//	List<PostDto> getAllPost(Integer pageNumber, Integer pageSize);
	PostResponse getAllPosts(Integer pageNumber, Integer pageSize, String sortBy);

	PostDto getPostById(Integer postId);

	//	List<PostDto> getPostByUser(Integer userId);

	PostResponse getPostByUserId(Integer userId, Integer pageNumber, Integer pageSize, String sortBy);

	//	List<PostDto> getPostByCategory(Integer categoryId);
	PostResponse getPostByCategoryId(Integer categoryId,Integer pageNumber, Integer pageSize, String sortBy);

	List<PostDto> searchPost(String keyword);
	
	void updateCategoryId(Integer categoryId);
	
	List<PostDto> getAllPosts();
}
