package com.blog.services;

import java.util.List;

import com.blog.payloads.UserDto;

public interface UserService {
	
	UserDto registerNewUser(UserDto user);
	
	UserDto createUser (UserDto user);
	
	UserDto updateUser (UserDto user, Integer uid);
	
	UserDto getUserById (Integer uid);
	
	List<UserDto> getAllUsers();
	
	void deleteUser(Integer uid);
}
