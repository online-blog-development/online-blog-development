package com.blog.services;

import com.blog.payloads.CommentsDto;


public interface CommentsService {
	CommentsDto createComment(CommentsDto commentDto, Integer postId, Integer uid);
	
	void deleteComment(Integer commentId);

}
