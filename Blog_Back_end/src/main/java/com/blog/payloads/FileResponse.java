package com.blog.payloads;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileResponse {
	
	String message;
	String fileName;
	
	public FileResponse(String message, String fileName) {
		
		this.message = message;
		this.fileName = fileName;
	}
	
	

}
