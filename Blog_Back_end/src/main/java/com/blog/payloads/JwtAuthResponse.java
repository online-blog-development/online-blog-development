package com.blog.payloads;

import java.util.Set;

import com.blog.pojos.Role;

public class JwtAuthResponse {
	private String token;
	private Integer userId;
	private String uname;
	private Set<Role> roles;
		
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	
		
	
	
	
}
