package com.blog.payloads;

import java.time.LocalDateTime;

public class CommentsDto {
	
	private Integer cmt_id ;	
//	private String commentator ;	
	private String content ;	
	private LocalDateTime create_time ;
	private UserDto user;
	
	
	public Integer getCmt_id() {
		return cmt_id;
	}
	public void setCmt_id(Integer cmt_id) {
		this.cmt_id = cmt_id;
	}
//	public String getCommentator() {
//		return commentator;
//	}
//	public void setCommentator(String commentator) {
//		this.commentator = commentator;
//	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public LocalDateTime getCreate_time() {
		return create_time;
	}
	public void setCreate_time(LocalDateTime create_time) {
		this.create_time = create_time;
	}
	public UserDto getUser() {
		return user;
	}
	public void setUser(UserDto user) {
		this.user = user;
	}
		
}
