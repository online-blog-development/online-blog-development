package com.blog.serviceImpl;

import java.time.LocalDateTime;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blog.exceptions.ResourceNotFoundException;
import com.blog.payloads.CommentsDto;
import com.blog.pojos.Comments;
import com.blog.pojos.Post;
import com.blog.pojos.User;
import com.blog.repositories.CommentsRepo;
import com.blog.repositories.PostRepo;
import com.blog.repositories.UserRepo;
import com.blog.services.CommentsService;

@Service
public class CommentsServiceImpl implements CommentsService {

	@Autowired
	private PostRepo postRepo ;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private CommentsRepo commentsRepo ;

	@Autowired
	private ModelMapper modelmapper;
	@Override
	public CommentsDto createComment(CommentsDto commentDto, Integer postId, Integer uid) {
		// TODO Auto-generated method stub

		Post post=this.postRepo.findById(postId).orElseThrow(()->new ResourceNotFoundException("Post", "post_id", postId));
		User user = this.userRepo.findById(uid).orElseThrow(()->new ResourceNotFoundException("User", "user_id", uid));

		Comments cmnt=this.modelmapper.map(commentDto, Comments.class);
		cmnt.setPost(post);
		cmnt.setUser(user);
		cmnt.setCreate_time(LocalDateTime.now());

		Comments savedComment=this.commentsRepo.save(cmnt);

		return this.modelmapper.map(savedComment, CommentsDto.class);
	}

	@Override
	public void deleteComment(Integer commentId) {
		// TODO Auto-generated method stub

		Comments cmnt=this.commentsRepo.findById(commentId).orElseThrow(()->new ResourceNotFoundException("Comment", "CommentId", commentId));
		this.commentsRepo.delete(cmnt);

	}

}
