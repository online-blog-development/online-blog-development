package com.blog.serviceImpl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.blog.config.AppConstants;
import com.blog.exceptions.ResourceNotFoundException;
import com.blog.payloads.UserDto;
import com.blog.pojos.Role;
import com.blog.pojos.User;
import com.blog.repositories.RoleRepo;
import com.blog.repositories.UserRepo;
import com.blog.services.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepo userRepo;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private RoleRepo roleRepo;

	@Override
	public UserDto createUser(UserDto userDto) {
		// TODO Auto-generated method stub
		User user = this.dtoToUser(userDto);
		user.setRegDate(LocalDate.now());
		User saveUser = this.userRepo.save(user);
		return this.userToDto(saveUser);
	}

	@Override
	public UserDto updateUser(UserDto userDto, Integer uid) {
		// TODO Auto-generated method stub
		User user = this.userRepo.findById(uid).orElseThrow(() -> new ResourceNotFoundException("User", "Id", uid));

		user.setUname(userDto.getUname());
		user.setEmail(userDto.getEmail());
		user.setPassword(userDto.getPassword());
//		user.setRoles(userDto.getRoles());
		user.setDob(userDto.getDob());
		user.setAvatar(userDto.getAvatar());

		User updateUser = this.userRepo.save(user);
		UserDto userDto1 = this.userToDto(updateUser);
		return userDto1;
	}

	@Override
	public UserDto getUserById(Integer uid) {
		// TODO Auto-generated method stub
		User user = this.userRepo.findById(uid).orElseThrow(() -> new ResourceNotFoundException("User", "Id", uid));
		return this.userToDto(user);
	}

	@Override
	public List<UserDto> getAllUsers() {
		// TODO Auto-generated method stub
		List<User> users = this.userRepo.findAll();

		List<UserDto> userDtos = users.stream().map(user -> this.userToDto(user)).collect(Collectors.toList());

		return userDtos;
	}

	@Override
	public void deleteUser(Integer uid) {
		// TODO Auto-generated method stub
		User user = this.userRepo.findById(uid).orElseThrow(() -> new ResourceNotFoundException("User", "Id", uid));
		this.userRepo.delete(user);
	}

	public User dtoToUser(UserDto userDto) {
		User user = this.modelMapper.map(userDto, User.class);
		// user.setUid(userDto.getUid());
		// user.setUname(userDto.getUname());
		// user.setEmail(userDto.getEmail());
		// user.setPassword(userDto.getPassword());
		// user.setRole(userDto.getRole());
		// user.setDob(userDto.getDob());
		// user.setAvatar(userDto.getAvatar());
		return user;
	}

	public UserDto userToDto(User user) {
		UserDto userDto = this.modelMapper.map(user, UserDto.class);
		// userDto.setUid(user.getUid());
		// userDto.setUname(user.getUname());
		// userDto.setEmail(user.getEmail());
		// userDto.setPassword(user.getPassword());
		// userDto.setRole(user.getRole());
		// userDto.setAvatar(user.getAvatar());
		// userDto.setDob(user.getDob());
		return userDto;
	}

	@Override
	public UserDto registerNewUser(UserDto userDto) {
		User user = this.modelMapper.map(userDto, User.class);
		
		//Password encoded
		user.setPassword(this.passwordEncoder.encode(user.getPassword()));
		
		//roles
		Role role = this.roleRepo.findById(AppConstants.NORMAL_USER).get();
		
		user.getRoles().add(role);
		
		user.setRegDate(LocalDate.now());
		
		user.setAvatar("profile_pic.jpeg");
		
		User newUser = this.userRepo.save(user);
		return this.modelMapper.map(newUser, UserDto.class);
	}

}
