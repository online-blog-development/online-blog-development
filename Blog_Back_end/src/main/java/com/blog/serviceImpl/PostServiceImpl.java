package com.blog.serviceImpl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.blog.exceptions.ResourceNotFoundException;
import com.blog.payloads.PostDto;
import com.blog.payloads.PostResponse;
import com.blog.pojos.Category;
import com.blog.pojos.Post;
import com.blog.pojos.User;
import com.blog.repositories.CategoryRepo;
import com.blog.repositories.PostRepo;
import com.blog.repositories.UserRepo;
import com.blog.services.PostService;

@Service
public class PostServiceImpl implements PostService {

	@Autowired
	private PostRepo postRepo;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private CategoryRepo categoryRepo;

	// POST - Create Post
	@Override
	public PostDto createPost(PostDto postDto, Integer userId, Integer categoryId) {

		User user = this.userRepo.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User ", "User Id", userId));

		Category category = this.categoryRepo.findById(categoryId)
				.orElseThrow(() -> new ResourceNotFoundException("Category ", "Category Id", categoryId));

		Post post = this.modelMapper.map(postDto, Post.class);
		post.setImageName("default.jpg");
		post.setAddedDate(LocalDateTime.now());
		post.setUser(user);
		post.setCategory(category);

		Post newPost = this.postRepo.save(post);

		return this.modelMapper.map(newPost, PostDto.class);
	}

	// PUT - update posts
	@Override
	public PostDto updatePost(PostDto postDto, Integer postId) {
		Post post = this.postRepo.findById(postId)
				.orElseThrow(() -> new ResourceNotFoundException("Post", "post id", postId));
		post.setTitle(postDto.getTitle());
		post.setContent(postDto.getContent());
		post.setImageName(postDto.getImageName());

		Post updatedPost = this.postRepo.save(post);
		return this.modelMapper.map(updatedPost, PostDto.class);
	}

	// DELETE - Delete Post
	@Override
	public void deletePost(Integer postId) {
		Post post = this.postRepo.findById(postId)
				.orElseThrow(() -> new ResourceNotFoundException("Post", "post id", postId));

		this.postRepo.delete(post);
	}

	// GET - Get all posts
	@Override
	public PostResponse getAllPosts(Integer pageNumber, Integer pageSize, String sortBy) {
		Pageable p = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy).descending());
		Page<Post> pagePosts = this.postRepo.findAll(p);

		List<Post> allPosts = pagePosts.getContent();
		List<PostDto> postDtos = allPosts.stream().map((post) -> this.modelMapper.map(post, PostDto.class))
				.collect(Collectors.toList());

		PostResponse pr = new PostResponse();
		pr.setContent(postDtos);
		pr.setPageNumber(pagePosts.getNumber());
		pr.setPageSize(pagePosts.getSize());
		pr.setTotalElements(pagePosts.getTotalElements());
		pr.setTotalPages(pagePosts.getTotalPages());
		pr.setLastPage(pagePosts.isLast());
		return pr;
	}

	// GET - Get posts by postId
	@Override
	public PostDto getPostById(Integer postId) {
		Post post = this.postRepo.findById(postId)
				.orElseThrow(() -> new ResourceNotFoundException("Post", "post id", postId));
		return this.modelMapper.map(post, PostDto.class);
	}

	// Get - Get posts by UserId
	@Override
	public PostResponse getPostByUserId(Integer userId, Integer pageNumber, Integer pageSize, String sortby) {
		User user = this.userRepo.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User ", "User id ", userId));
		Pageable p = PageRequest.of(pageNumber, pageSize, Sort.by(sortby).descending());
		Page<Post> posts = this.postRepo.findByUser(user, p);

		List<Post> allPosts = posts.getContent();
		List<PostDto> postDtos = allPosts.stream().map((post) -> this.modelMapper.map(post, PostDto.class))
				.collect(Collectors.toList());

		PostResponse pr = new PostResponse();
		pr.setContent(postDtos);
		pr.setPageNumber(posts.getNumber());
		pr.setPageSize(posts.getSize());
		pr.setTotalElements(posts.getTotalElements());
		pr.setTotalPages(posts.getTotalPages());
		pr.setLastPage(posts.isLast());
		return pr;
	}

	// GET - Get posts by Category
	@Override
	public PostResponse getPostByCategoryId(Integer categoryId, Integer pageNumber, Integer pageSize, String sortBy) {
		Category cat = this.categoryRepo.findById(categoryId)
				.orElseThrow(() -> new ResourceNotFoundException("Category ", "category id ", categoryId));
		Pageable p = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy).descending());
		Page<Post> postByCayegory = this.postRepo.findByCategory(cat, p);

		List<Post> posts = postByCayegory.getContent();
		List<PostDto> postDtos = posts.stream().map((post) -> this.modelMapper.map(post, PostDto.class))
				.collect(Collectors.toList());

		PostResponse pr = new PostResponse();
		pr.setContent(postDtos);
		pr.setPageNumber(postByCayegory.getNumber());
		pr.setPageSize(postByCayegory.getSize());
		pr.setTotalElements(postByCayegory.getTotalElements());
		pr.setTotalPages(postByCayegory.getTotalPages());
		pr.setLastPage(postByCayegory.isLast());
		return pr;
	}

	// Search Post
	@Override
	public List<PostDto> searchPost(String keyword) {
		List<Post> posts = this.postRepo.findByTitleContaining(keyword);
		List<PostDto> postDto = posts.stream().map((post) -> this.modelMapper.map(post, PostDto.class))
				.collect(Collectors.toList());
		return postDto;
	}

	@Override
	public void updateCategoryId(Integer categoryId) {
		Category cat = this.categoryRepo.findById(categoryId)
				.orElseThrow(() -> new ResourceNotFoundException("Category", "category Id", categoryId));
		List<Post> posts = this.postRepo.findByCategory(cat);
		Category other = this.categoryRepo.findCategoryOther();

		for (Post p : posts) {
			p.setCategory(other);
			this.postRepo.save(p);
		}

	}

	@Override
	public List<PostDto> getAllPosts() {
		List<Post> posts = this.postRepo.findAll();
		List<PostDto> postDto = posts.stream().map((post) -> this.modelMapper.map(post, PostDto.class))
				.collect(Collectors.toList());

		return postDto;
	}

}
