package com.blog.pojos;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@Entity
@Table(name="comments")
public class Comments {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer cmt_id ;
	
//	@Column(name="commentator",length = 20,nullable = false)
//	private String commentator ;
	
	@Column(name="content",length = 200,nullable = false )
	private String content ;

	@CreatedDate
	@Column(name="creation_time",nullable=false, updatable = false)
	private LocalDateTime create_time ;
	
	@OneToOne(fetch = FetchType.EAGER)
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "post_id")
	private Post post;
	
	public Integer getCmt_id() {
		return cmt_id;
	}

	public void setCmt_id(Integer cmt_id) {
		this.cmt_id = cmt_id;
	}

//	public String getCommentator() {
//		return commentator;
//	}
//
//	public void setCommentator(String commentator) {
//		this.commentator = commentator;
//	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDateTime getCreate_time() {
		return create_time;
	}

	public void setCreate_time(LocalDateTime create_time) {
		this.create_time = create_time;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	

	
	
}
