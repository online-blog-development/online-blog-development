import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardText,
  Col,
  Container,
  Input,
} from "reactstrap";
import Base from "../components/Base";
import { Row } from "reactstrap";
import { loadPost } from "../services/post-service";
import { toast } from "react-toastify";
import { BASE_URL } from "../services/helper";
import { createComment } from "../services/comment-service";
import isLoggedIn, { getCurrentUserDetail } from "../Auth";

const PostPage = () => {
  const { postId } = useParams();
  const [post, setPost] = useState(null);
  const [comment, setComment] = useState({
    content: "",
  });
  const [uid, setUserId] = useState(undefined);

  useEffect(() => {
    setUserId(getCurrentUserDetail());
    //load post of postId
    loadPost(postId)
      .then((data) => {
        console.log(data);
        setPost(data);
      })
      .catch((error) => {
        console.log(error);
        toast.error("Error in loading post");
      });
  }, []);

  const printDate = (numbers) => {
    return new Date(numbers).toDateString();
  };

  const commentDate = (numbers) => {
    return new Date(numbers).toLocaleString();
  };

  const submitPost = () => {
    if (!isLoggedIn()) {
      toast.error("Login to add comments");
      return;
    }
    if (comment.content.trim() === "") {
      return;
    }
    createComment(comment, uid, post.postId).then((data) => {
      console.log(data);
      toast.success("Comment added..");
      setPost({ ...post, comments: [...post.comments, data.data] });
    });
    setComment({
      content: "",
    }).catch((error) => {
      console.log(error);
    });
  };

  return (
    <Base>
      <Container className="mt-2">
        <Link style={{ fontFamily: "Romantic" }} to="/">Home</Link> / {post && <Link to="">{post.title}</Link>}
        <Row>
          <Col md={{ size: 12 }}>
            <Card className="mt-2 border-0 ps-2 rounded-0 shadow-lg">
              <CardBody>
                <CardText style={{ fontFamily: "Romantic" }}>
                  Published By <b>{post?.user?.uname}</b> on{" "}
                  <b>{printDate(post?.addedDate)}</b>
                </CardText>

                <CardText style={{fontFamily: "BRADLY HAND ITC"}}>
                  <h5>
                    <span className="text-muted">
                      Category: <b>{post?.category?.categoryTitle}</b>
                    </span>
                  </h5>
                </CardText>

                <CardText style={{fontFamily: "BRADLY HAND ITC"}}>
                  <h4>
                    <span className="text-muted">
                      Title: <b>{post?.title}</b>
                    </span>
                  </h4>
                </CardText>

                <div
                  className="divider"
                  style={{
                    width: "100%",
                    height: "1px",
                    background: "#e2e2e2",
                  }}
                ></div>

                <div
                  className=" image-container rounded-1 container shadow-lg mt-1 "
                  style={{ maxWidth: "50%"}}
                >
                  <img
                    className="img-fluid "
                    src={BASE_URL + `/post/` + postId + `/image`}
                    alt=""
                  ></img>
                </div>

                <CardText
                style={{fontFamily: "TimesNewRoman", fontSize: "20px"}}
                  className="mt-4"
                  dangerouslySetInnerHTML={{ __html: post?.content }}
                ></CardText>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row className="my-4">
          <Col md={{ size: 8, offset: 1 }}>
            <div>
              <h3 style={{ color: "navy", fontFamily: "TimesNewRoman" }}>
                Comments ({post ? post.comments.length : 0})
              </h3>
            </div>

            {post?.comments &&
              post?.comments.map((c, index) => (
                <Card className="mt-2" key={index}>
                  <CardBody>
                    <CardText style={{fontFamily: "TimesNewRoman"}}>
                      {" "}
                      <b>{c.user?.uname}</b>: {c.content}{" "}
                    </CardText>
                    {commentDate(c.create_time)}
                  </CardBody>
                </Card>
              ))}

            <Card className="mt-2">
              <CardBody style={{fontFamily: "TimesNewRoman"}}>
                <Input
                  type="textarea"
                  placeholder="Enter comments"
                  value={comment.content}
                  onChange={(event) =>
                    setComment({ content: event.target.value })
                  }
                ></Input>
                <Button className="mt-2" color="primary" onClick={submitPost}>
                  Submit
                </Button>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </Base>
  );
};

export default PostPage;
