import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { Button, Table } from "reactstrap";
import Base from "../../../components/Base";
import { myAxios, privateAxios } from "../../../services/helper";
import { toast } from "react-toastify";

function Users() {
  const [users, setUsers] = useState([]);

  const fetchUsers = () => {
   
    return myAxios
    .get(`/users/`)
    .then(response => {
                setUsers(response.data);
                console.log(response.data);
})};

  useEffect(() => {
    fetchUsers();
  }, []);

  const deleteUser = (uid) => {
    return privateAxios.delete(`/users/${uid}`)
    .then(response=>{
        fetchUsers();
    }).then(toast.success("User deleted successfully"))

  };

  return (
    <Base>
      <div className="d-flex flex-nowrap">
        <div
          className="d-flex flex-column flex-shrink-0 p-3 text-bg-dark"
          style={{ width: "280px", height: "100vh" }}
        >
          <Link
            to="/user/admin"
            className="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none"
          >
            <svg className="bi pe-none me-2" width={40} height={32}>
              <use xlinkHref="#bootstrap" />
            </svg>
            <span className="fs-5">Admin Dashboard</span>
          </Link>
          <hr />
          <ul className="nav nav-pills flex-column mb-auto">
            <li className="nav-item">
              <Link
                to="/user/admin"
                className="nav-link text-white"
                aria-current="page"
              >
                <svg className="bi pe-none me-2" width={16} height={16}>
                  <use xlinkHref="#home" />
                </svg>
                Dashboard
              </Link>
            </li>
            <li>
              <Link to="/user/admin/users" className="nav-link active">
                <svg className="bi pe-none me-2" width={16} height={16}>
                  <use xlinkHref="#speedometer2" />
                </svg>
                Users
              </Link>
            </li>
            <li>
              <Link to="/user/admin/category" className="nav-link text-white">
                <svg className="bi pe-none me-2" width={16} height={16}>
                  <use xlinkHref="#table" />
                </svg>
                Category
              </Link>
            </li>
           
          </ul>
          <hr />
        </div>

        <div
          style={{ margin: "0px 0px 0px 20px" }}
          className="container text-center"
        >
          <Table dark bordered responsive size="" striped className="caption-top">
            <caption><h3>List of users</h3></caption>
            <thead>
              <tr>
                <th>User ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>DOB</th>
                <th>Registered Date</th>
                <th>Role</th>
                <td></td>
              </tr>
            </thead>
            <tbody>
              {users.map((user) => (
                <tr key={user.uid}>
                  <th>{user.uid}</th>
                  <td>{user.uname}</td>
                  <td>{user.email}</td>
                  <td>{user.dob}</td>
                  <td>{user.regDate}</td>
                  <td>{user.roles.map(role=>(role.name))}</td>
                  <td>
                    <Button
                      color="danger"
                      onClick={() => {
                        deleteUser(user.uid);
                      }}
                    >
                      Delete
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    </Base>
  );
}

export default Users;
