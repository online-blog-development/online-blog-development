import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Base from "../../../components/Base";
import { myAxios, privateAxios } from "../../../services/helper";

function Dashboard() {
  const [users, setUsers] = useState([]);
  const [categories, setCategories] = useState([]);
  const [posts, setPosts] = useState([]);
  const [comments, setcomments] = useState(0);

  const fetchUsers = () => {
    return myAxios.get(`/users/`).then((response) => {
      setUsers(response.data);
      console.log(users);
    });
  };

  const fetchCategories = () => {
    return myAxios
      .get(`/categories/`)
      .then((response) => {
        setCategories(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const fetchPosts = () => {
    return myAxios
      .get(`/post/`)
      .then((response) => {
        // debugger;
        setPosts(response.data);
        // totalComments();
        return response.data;
      })
      .then((posts) => {
        let count = 0;
        // debugger;
        posts.map((post) => (count = count + post.comments.length));
        setcomments(count);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const totalComments = () => {
    let count = 0;
    debugger;
    posts.map((post) => (count = count + post.comments.length));
    setcomments(count);
  };

  useEffect(() => {
    fetchPosts();
    fetchUsers();
    fetchCategories();
  }, []);

  return (
    <Base>
      <div className="d-flex flex-nowrap">
        <div
          className="d-flex flex-column flex-shrink-0 p-3 text-bg-dark"
          style={{ width: "280px", height: "100vh" }}
        >
          <Link
            to="/user/admin"
            className="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none"
          >
            <svg className="bi pe-none me-2" width={40} height={32}>
              <use xlinkHref="#bootstrap" />
            </svg>
            <span className="fs-5">Admin Dashboard</span>
          </Link>
          <hr />
          <ul className="nav nav-pills flex-column mb-auto">
            <li className="nav-item">
              <Link
                to="/user/admin"
                className="nav-link active"
                aria-current="page"
              >
                <svg className="bi pe-none me-2" width={16} height={16}>
                  <use xlinkHref="#home" />
                </svg>
                Dashboard
              </Link>
            </li>
            <li>
              <Link to="/user/admin/users" className="nav-link text-white">
                <svg className="bi pe-none me-2" width={16} height={16}>
                  <use xlinkHref="#speedometer2" />
                </svg>
                Users
              </Link>
            </li>
            <li>
              <Link to="/user/admin/category" className="nav-link text-white">
                <svg className="bi pe-none me-2" width={16} height={16}>
                  <use xlinkHref="#table" />
                </svg>
                Category
              </Link>
            </li>
            <li>
              {/* <Link
                            to="/admin/feedback"
                            className="nav-link text-white"
                        >
                            <svg
                                className="bi pe-none me-2"
                                width={16}
                                height={16}
                            >
                                <use xlinkHref="#grid" />
                            </svg>
                            Feedback
                        </Link> */}
            </li>
          </ul>
          <hr />
        </div>

        <div style={{ margin: "0px 0px 0px 20px" }} className="container mt-4">
          <div class="card text-center">
            <div class="card-header">
              <h3>
                <b>Statistics</b>
              </h3>
            </div>
            <div class="card-body">
              <h5 class="card-title">Number of user of the Website</h5>
              <p class="card-text">{users.length}</p>
              <h5 class="card-title">Total number of blog posted</h5>
              <p class="card-text">{posts.length}</p>
              <h5 class="card-title">Number of Blog categories</h5>
              <p class="card-text">{categories.length}</p>
              <h5 class="card-title">Total number of comments</h5>
              <p class="card-text">{comments}</p>
            </div>
          </div>
        </div>
      </div>
    </Base>
  );
}

export default Dashboard;
