import { Link } from "react-router-dom";

function Feedback() {
    return (<div className="d-flex flex-nowrap"> 
        
            <div
                className="d-flex flex-column flex-shrink-0 p-3 text-bg-dark"
                style={{ width: "280px" , height: "100vh"}}
            >
                <Link
                    to="/admin"
                    className="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none"
                >
                    <svg className="bi pe-none me-2" width={40} height={32}>
                        <use xlinkHref="#bootstrap" />
                    </svg>
                    <span className="fs-5">Admin Dashboard</span>
                </Link>
                <hr />
                <ul className="nav nav-pills flex-column mb-auto">
                    <li className="nav-item">
                        <Link
                            to="/admin"
                            className="nav-link text-white"
                            aria-current="page"
                        >
                            <svg
                                className="bi pe-none me-2"
                                width={16}
                                height={16}
                            >
                                <use xlinkHref="#home" />
                            </svg>
                            Dashboard
                        </Link>
                    </li>
                    <li>
                        <Link to="/admin/users" className="nav-link text-white">
                            <svg
                                className="bi pe-none me-2"
                                width={16}
                                height={16}
                            >
                                <use xlinkHref="#speedometer2" />
                            </svg>
                            Users
                        </Link>
                    </li>
                    <li>
                        <Link to="/admin/category" className="nav-link text-white">
                            <svg
                                className="bi pe-none me-2"
                                width={16}
                                height={16}
                            >
                                <use xlinkHref="#table" />
                            </svg>
                            Category
                        </Link>
                    </li>
                    <li>
                        <Link to="/admin/feedback" className="nav-link active">
                            <svg
                                className="bi pe-none me-2"
                                width={16}
                                height={16}
                            >
                                <use xlinkHref="#grid" />
                            </svg>
                            Feedback
                        </Link>
                    </li>
                </ul>
                <hr />
            </div>

            <div style={{ margin: "0px 0px 0px 20px" }}> <h1>Hi</h1></div>
        </div>
    );
}

export default Feedback;
