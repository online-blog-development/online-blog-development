import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { Button, Form, Input, Table } from "reactstrap";
import Base from "../../../components/Base";
import { myAxios, privateAxios } from "../../../services/helper";
import { toast } from "react-toastify";

function Category() {
  const [categories, setCategories] = useState([]);

  const fetchCategories = () => {
    return myAxios
      .get(`/categories/`)
      .then((response) => {
        setCategories(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchCategories();
  }, []);

  const deleteCategory = (cid) => {
    return privateAxios.delete(`/categories/${cid}`).then((response) => {
      fetchCategories();
    });
  };

  const [category, setCategory] = useState({
    categoryTitle: "",
    categoryDescription: "",
  });

  const handleChange = (args) => {
    let copyOfCategory = {...category}
    copyOfCategory[args.target.name] = args.target.value;
    setCategory(copyOfCategory);
  };

  const addCategory = () => {
    return privateAxios.post(`/categories/`,category)
        .then(response=>{
            console.log(response.data)
            setCategory({
                categoryTitle: "",
                categoryDescription: "",
              })
            fetchCategories();
        })
  };

  return (
    <Base>
      <div className="d-flex flex-nowrap">
        <div
          className="d-flex flex-column flex-shrink-0 p-3 text-bg-dark"
          style={{ width: "280px", height: "100vh" }}
        >
          <Link
            to="/user/admin"
            className="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none"
          >
            <svg className="bi pe-none me-2" width={40} height={32}>
              <use xlinkHref="#bootstrap" />
            </svg>
            <span className="fs-5">Admin Dashboard</span>
          </Link>
          <hr />
          <ul className="nav nav-pills flex-column mb-auto">
            <li className="nav-item">
              <Link
                to="/user/admin"
                className="nav-link text-white"
                aria-current="page"
              >
                <svg className="bi pe-none me-2" width={16} height={16}>
                  <use xlinkHref="#home" />
                </svg>
                Dashboard
              </Link>
            </li>
            <li>
              <Link to="/user/admin/users" className="nav-link text-white">
                <svg className="bi pe-none me-2" width={16} height={16}>
                  <use xlinkHref="#speedometer2" />
                </svg>
                Users
              </Link>
            </li>
            <li>
              <Link to="/user/admin/category" className="nav-link active">
                <svg className="bi pe-none me-2" width={16} height={16}>
                  <use xlinkHref="#table" />
                </svg>
                Category
              </Link>
            </li>
            <li>
              {/* <Link
                            to="/admin/feedback"
                            className="nav-link text-white"
                        >
                            <svg
                                className="bi pe-none me-2"
                                width={16}
                                height={16}
                            >
                                <use xlinkHref="#grid" />
                            </svg>
                            Feedback
                        </Link> */}
            </li>
          </ul>
          <hr />
        </div>

        <div
          style={{ margin: "0px 0px 0px 20px" }}
          className="container text-center"
        >
          <Table dark striped bordered responsive hover className="caption-top " >
            <caption><h3>List of categories</h3></caption>
            <thead>
              <tr>
                <th>Categor ID</th>
                <th>Category Name</th>
                <th>Description</th>
                <td></td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              {categories.map((cat) => (
                <tr>
                  <th>{cat.categoryId}</th>
                  <td>{cat.categoryTitle}</td>
                  <td>{cat.categoryDescription}</td>
                  <td>
                    {cat.categoryTitle.toLowerCase() != "others" ? (
                      <Button
                        color="danger"
                        onClick={() => {
                          deleteCategory(cat.categoryId);
                        }}
                      >
                        Delete
                      </Button>
                    ) : (
                      ""
                    )}
                  </td>
                  <td>
                    <Button color="secondary">Edit</Button>
                  </td>
                </tr>
              ))}
            </tbody>
            <tfoot>
              <tr></tr>
            </tfoot>
          </Table>
          <Form>
            <Table bordered responsive hover className="caption-top">
              <tbody>
                <tr>
                  {/* <td>#</td> */}
                  <td>
                    <Input
                      type="text"
                      name="categoryTitle"
                      placeholder="Enter category title"
                      onChange={handleChange}
                      value = {category.categoryTitle}
                    ></Input>
                  </td>
                  <td>
                    <Input
                      type="text"
                      placeholder="Enter description"
                      name="categoryDescription"
                      onChange={handleChange}
                      value = {category.categoryDescription}
                    ></Input>
                  </td>
                  <td>
                    <Button
                      onClick={() => {
                        addCategory();
                      }}
                      className="info"
                    >
                      Add Category
                    </Button>
                  </td>
                </tr>
              </tbody>
            </Table>
          </Form>
        </div>
      </div>
    </Base>
  );
}

export default Category;
