import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { Container } from 'reactstrap'
import { getCurrentUserDetail } from '../../Auth'
import AddPost from '../../components/AddPost'
import Base from '../../components/Base'
import { deletePostService, loadPostUserWise } from '../../services/post-service'

const UserDashboard = () => {

  const [user, setUser] = useState({})
  const [posts, setPosts] = useState([])
  useEffect(() => {
    console.log(getCurrentUserDetail());
    setUser(getCurrentUserDetail())
    loadPostData()


  }, [])


  function loadPostData() {
    loadPostUserWise(getCurrentUserDetail()).then(data => {
      // debugger
      console.log(data)
      setPosts([...data.content])
    }).catch(error => {
      console.log(error)
      toast.error("error in loading posts")

    })
  }


  //function to delete post
  function deletePost(post) {
    //going to delete post
    console.log(post)
    deletePostService(post.postId).then(res => {
      console.log(res)
      toast.success("Post is deleted...")
      let newPosts = posts.filter(p => p.postId != post.postId)
      setPosts([...newPosts])
    }).catch(error => {
      console.log(error)
      toast.error("error in deleting post")
    })

  }


  return (
    <Base>

      <Container>
        <AddPost />
        {/* <h1 className='my-3'>Posts Count: {posts.length}</h1>

        {posts.map((post, index) => {
          return (
            <post post={post} key={index} deletePost={deletePost}></post>
          )
        })} */}
      </Container>

    </Base>
  )
}

export default UserDashboard