import { useContext, useState } from 'react'
import React from 'react'
import Base from '../../components/Base'
import { useParams } from 'react-router-dom'
import { getUser } from '../../services/user-service'
import ViewUserProfile from '../../components/ViewUserProfile'
import { Col, Row } from 'reactstrap'
import { useEffect } from 'react'
// import userContext from '../../context/userContext'


const ProfileInfo = () => {
    // const object=useContext(userContext)

    const [user,setUser]=useState(null)

    const { userId }= useParams()
    //console.log(userId);

    useEffect(()=>{
        getUser(userId).then(data=>{
            console.log(data);
            setUser({...data})
        })
    },[])

    const userView=()=>{
        return (          
            <Row>
            <Col md={{ size:6, offset:3 }}>
            
            <ViewUserProfile user={user}></ViewUserProfile>
            </Col>
        </Row>

            )
        
    }

    return (
        <Base>
        {user ? userView() : 'Loading user Data' }
        </Base>
    )
}
export default ProfileInfo