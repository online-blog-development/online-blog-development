import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Base from "../components/Base";
import { Container, Row, Col } from "reactstrap";
import CategorySideMenu from "../components/CategorySideMenu";
import { deletePostService } from '../services/post-service'
import { toast } from "react-toastify";
import Post from "../components/Post";
import { loadPostCategoryWise } from "../services/category-service";

function Categories() {
  const [posts, setPosts] = useState([]);

  const { categoryId } = useParams();
  useEffect(() => {
    console.log(categoryId);
    loadPostCategoryWise(categoryId)
      .then((data) => {
        // debugger
        setPosts([...data.content]);
      })
      .catch((error) => {
        console.log(error);
        toast.error("error in loading posts");
      });
  }, [categoryId]);

  //function to delete post
  function deletePost(post) {
    //going to delete post
    console.log(post);
    deletePostService(post.postId)
      .then((res) => {
        console.log(res);
        toast.success("Post is deleted...");
        let newPosts = posts.filter((p) => p.postId != post.postId);
        setPosts([...newPosts]);
      })
      .catch((error) => {
        console.log(error);
        toast.error("error in deleting post");
      });
  }

  return (
    <Base>
      <Container className="mt-2">
        <Row>
          <Col md={2} className="pt-5">
            <CategorySideMenu></CategorySideMenu>
          </Col>
          <Col md={10}>
          <div>
            <h2
              style={{
                color: "navy",
                fontFamily: "Romantic",
                textAlign: "center",
              }}
            >
              Blog Feed
            </h2>
            <b
              style={{
                textAlign: "center",
                fontFamily: "Romantic",
                color: "black",
              }}
            >
              Blogs Count ( {posts.length} )
            </b>
          </div>
            {posts &&
              posts.map((post, index) => {
                return (
                  <Post deletePost={deletePost} key={index} post={post}></Post>
                );
              })}
            {posts.length <= 0 ? <h1>No posts in this category</h1> : ""}
          </Col>
        </Row>
      </Container>
    </Base>
  );
}

export default Categories;
