import { useEffect, useState } from "react";
import { Button, Col, FormFeedback, Row } from "reactstrap";
import { ButtonGroup, Card, CardBody, CardHeader, Container, Form, FormGroup, Input, Label } from "reactstrap";
import Base from "../components/Base";
import { signUp } from "../services/user-service";
import { toast } from "react-toastify";

const Signup = () => {

    const [data, setData] = useState({
        uname: '',
        email: '',
        password: '',
        dob: '',
        avatar: '',
        regDate: ''
    })

    const [error, setError] = useState({
        errors: {},
        isError: false
    })


    useEffect(() => {
        // console.log(data);
    }, [data])

    //handle change
    const handleChange = (event, property) => {
        //dynamic setting the values
        setData({ ...data, [property]: event.target.value })

    }

    const resetData = () => {
        setData({
            uname: '',
            email: '',
            password: '',
            dob: '',
            avatar: '',
            regDate: ''
        })
    }

    const submitForm = (event) => {
        // setData({...data, regDate: Date.now()})
        event.preventDefault();

        if (error.isError) {
            toast.error("Form data is invalid!");
            return;
        }
        console.log(data);

        //data validate

        //call server api for sending data
        signUp(data).then((resp) => {
            console.log(resp);
            console.log("success log");
            toast.success("User is registered successfully, User ID: " + resp.uid);
            setData({
                uname: '',
                email: '',
                password: '',
                dob: '',
                avatar: '',
                regDate: ''
            });


        }).catch((error) => {
            // debugger
            console.log("Error Log")

            setError({
                errors: error,
                isError: true
            })

        })
    }


    return (
        <Base>
            <Container>
                <Row className="mt-4">
                    <Col sm={{ size: 5, offset: 3 }}>
                        <Card color="dark" inverse  >
                            <CardHeader >
                                <h3> Sign Up </h3>
                            </CardHeader>

                            <CardBody>
                                <Form onSubmit={submitForm}>
                                    <FormGroup>
                                        <Label for="uname">Name</Label>
                                        <Input
                                            type="text"
                                            placeholder="Enter Your Name"
                                            id="uname"
                                            onChange={(e) => handleChange(e, 'uname')}
                                            value={data.name}
                                            invalid={error.errors?.response?.data?.uname ? true : false}
                                        ></Input>
                                        <FormFeedback>
                                            {error.errors?.response?.data?.uname ? true : false}
                                        </FormFeedback>
                                    </FormGroup>

                                    <FormGroup>
                                        <Label for="email">Email</Label>
                                        <Input type="email"
                                            placeholder="Enter Your Email"
                                            id="email"
                                            onChange={(e) => handleChange(e, 'email')}
                                            value={data.email}
                                            invalid={error.errors?.response?.data?.email ? true : false}
                                        ></Input>

                                        <FormFeedback>
                                            {error.errors?.response?.data?.email ? true : false}
                                        </FormFeedback>
                                    </FormGroup>

                                    <FormGroup>
                                        <Label for="password">Password</Label>
                                        <Input type="password"
                                            placeholder="Enter Password"
                                            id="password"
                                            onChange={(e) => handleChange(e, 'password')}
                                            value={data.password}
                                        ></Input>
                                    </FormGroup>

                                    {/* <FormGroup>
                                        <Label for="regDate">Registratoin Date</Label>
                                        <Input type="date"
                                            id="regDate"
                                            onChange={(e) => handleChange(e, 'regDate')}
                                            value={data.regDate}
                                        ></Input>
                                    </FormGroup> */}

                                    <FormGroup>
                                        <Label for="dob">Date of Birth</Label>
                                        <Input type="date"
                                            placeholder="Date of Birth"
                                            id="dob"
                                            onChange={(e) => handleChange(e, 'dob')}
                                            value={data.dob}
                                        />
                                    </FormGroup>

                                    <Container className="text-center">
                                        <Button
                                            outline
                                            color="light"
                                        // onClick={signUp}
                                        >
                                            Register
                                        </Button>

                                        <Button
                                            outline
                                            onClick={resetData}
                                            color="secondary"
                                            className="ms-2"
                                        >
                                            Reset
                                        </Button>
                                    </Container>
                                </Form>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

            </Container>

        </Base>

    );
};

export default Signup;