import React, {  useEffect, useState } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardLink,
  CardText,
  NavLink,
} from "reactstrap";
import isLoggedIn, { getCurrentUserDetail } from "../Auth";

function Post({
  post = {
    title: "This is default post title",
    content: "This is default post content",
    user : {}
  },
  deletePost,
}) {
  const[user, setUser] = useState(null)
  const[login, setLogin] = useState(null)

  useEffect(()=>{
    setUser(getCurrentUserDetail());
    setLogin(isLoggedIn())
  }, [])

  return (
    <Card className="shadow-lg mb-3 mt-3 border-0">
      <CardBody>
        <h2 style={{ fontFamily: "Romantic" }}>{post.title} </h2> Author:{" "}
        <b>{post.user.uname}</b>
        <CardText
          style={{ fontFamily: "TimesNewRoman" }}
          dangerouslySetInnerHTML={{
            __html: post.content.substring(0, 60) + ".....",
          }}
        ></CardText>
        <div>
          <Link
            style={{ fontFamily: "TimesNewRoman" }}
            className="btn btn-outline-info btn-sm"
            to={`/posts/` + post.postId}
          >
            Read more
          </Link>

          {login &&
          (user === post.user.uid ? (
            <Button onClick={(event)=>deletePost(post)}
            color='danger'
            className="ms-2">Delete</Button>
          ):(
            ""
          ))}

        </div>
      </CardBody>
    </Card>
  );
}

export default Post;
