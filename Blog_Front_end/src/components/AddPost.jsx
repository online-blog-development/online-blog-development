import React, { useEffect, useRef, useState } from "react";
import {
  Button,
  CardBody,
  Container,
  Form,
  Input,
  Label,
} from "reactstrap";
import { Card } from "reactstrap";
import { loadAllCategories } from "../services/category-service";
import JoditEditor from "jodit-react";
import { createPost as doCreatePost } from "../services/post-service";
import { getCurrentUserDetail } from "../Auth";
import { toast } from "react-toastify";
import { uploadImage } from "../services/image-service";
import { Navigate } from "react-router-dom";

//Add post
const AddPost = () => {
  const editor = useRef(null);
  // const [content, setContent] = useState('')

  const [categories, setCategories] = useState([]);
  const [userId, setUserId] = useState(undefined);

  const [post, setPost] = useState({
    title: "",
    content: "",
    categoryId: "",
    imageName: ""
  });

  const [ image, setImage ] = useState(null);

  useEffect(() => {
    setUserId(getCurrentUserDetail());
    loadAllCategories()
      .then((data) => {
        console.log(data);
        setCategories(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  //filedChange function
  const fieldChange = (event) => {
    // console.log(event)
    setPost({ ...post, [event.target.name]: event.target.value });
  };

  const contentFieldChange = (data) => {
    setPost({ ...post, content: data });
  };

  //Create Post
  const createPost = (event) => {
    event.preventDefault();
    // console.log("form submitted")
    console.log(post);
    if (post.title.trim() === "") {
      toast.error(`Title can't be empty!`);
      return;
    }
    if (post.content.trim() === "") {
      toast.error(`Post content can't be empty!`);
      return;
    }
    if (post.categoryId === ``) {
      toast.error(`Select category!`);
      return;
    }

    //Submit form (POST)
    // debugger;
    post["userId"] = userId;
    doCreatePost(post)
      .then(data => {
        console.log(data)
        uploadImage(image, data.data.postId)
          .then(data => {
            console.log(data)
            toast.success("Image Uploaded");            
          })
          .catch((error) => {
            toast.error("Image uploading failed!");
            console.log(error);
          });
        toast.success("Post created");
        // console.log(post);
        setPost({
          title: "",
          content: "",
          categoryId: "",
        });
      })
      .catch((error) => {
        toast.error("Post Creation failed!!!");
        console.log(error);
      });
  };
  const clearData = () => {
    setPost({
      content: "",
    });
  };

  const handleFileChange = (event) => {
    console.log(event.target.files[0]);
    setImage(event.target.files[0])
  };

  return (
    <div className="wrapper">
      <Card className="shadow-lg mt-4 border-0">
        <CardBody>
          <h3 style={{ fontFamily: "Romantic", color: "rgba(255, 0, 0, 0.5)" }}>
            <b>What's going on your mind?</b>
          </h3>

          <Form onSubmit={createPost}>
            <div className="my-3">
              <Label for="title" style={{ fontFamily: "Romantic" }}>
                {" "}
                <h5>Post Title</h5>
              </Label>
              <Input
                type="text"
                id="title"
                placeholder="Enter title"
                name="title"
                onChange={fieldChange}
              />
            </div>

            <div className="my-3">
              <Label for="content" style={{ fontFamily: "Romantic" }}>
                <h5>Post Content</h5>
              </Label>
              {/* <Input type='textarea'
                id='content'
                placeholder='Enter content'
                style={{ height: '250px' }}
              /> */}
              <JoditEditor
                ref={editor}
                value={post.content}
                // onChange={newContent => setContent(newContent)}
                onChange={contentFieldChange}
                placeholder
              />
            </div>

            <div className="mt-2" style={{ fontFamily: "Romantic" }}>
              <Label style={{ fontFamily: "Romantic" }} for="image" id="image">
                <h5>Select post image</h5>
              </Label>
              <Input type="file" onChange={handleFileChange}></Input>
            </div>

            <div className="my-3">
              <Label for="category" style={{ fontFamily: "Romantic" }}>
                <h5>Post Category</h5>
              </Label>
              <Input
                type="select"
                id="category"
                name="categoryId"
                onChange={fieldChange}
                defaultValue={0}
              >
                <option disabled value={0}>
                  --Select Category--
                </option>
                {categories.map((category) => (
                  <option key={category.categoryId} value={category.categoryId}>
                    {category.categoryTitle}
                  </option>
                ))}
              </Input>
            </div>
            <Container className="text-center">
              <Button type="submit" color="primary">
                Create Post
              </Button>
              <Button
                className="ms-2"
                color="secondary"
                outline
                onClick={clearData}
              >
                Clear Content
              </Button>
            </Container>
          </Form>
        </CardBody>
      </Card>
    </div>
  );
};

export default AddPost;
