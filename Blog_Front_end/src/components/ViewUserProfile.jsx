import React, { useEffect, useState } from "react";
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    Col,
    Container,
    Row,
    Table,
} from "reactstrap";
import isLoggedIn, { getCurrentUserDetail } from "../Auth";
import { BASE_URL } from "../services/helper";

function ViewUserProfile({ user }) {
    const [currentUser, setCurrentUser] = useState(null);
    const [login, setLogin] = useState(false);
    useEffect(() => {
        setCurrentUser(getCurrentUserDetail());
        setLogin(isLoggedIn());
    }, []);

    return (
        <Card className="mt-2 border-0 rounded-0 shadow-sm">
            <CardBody>
                <h3 className="text-uppercase">User Information</h3>
                <Container className="text-center">
                    <img
                        style={{ maxWidth: "200px", maxHeight: "200px" }}
                        src={BASE_URL + `/users/` + currentUser + `/getImage`}
                        alt="user profile picture"
                        className="img-fluid"
                        rounded-circle
                    ></img>
                </Container>
                <Table
                    responsive
                    striped
                    hover
                    bordered={true}
                    className="text-center mt-5"
                >
                    <tbody>
                        <tr>
                            <td>Blog User Id</td>
                            <td>{user?.uid}</td>
                        </tr>
                        <tr>
                            <td>USER NAME</td>
                            <td> {user?.uname}</td>
                        </tr>
                        <tr>
                            <td>USER EMAIL</td>
                            <td>{user?.email}</td>
                        </tr>
                        <tr>
                            <td>DOB</td>
                            <td>{user?.dob}</td>
                        </tr>
                        <tr>
                            <td>Registered Date</td>
                            <td>{user?.regDate}</td>
                        </tr>
                        {/* <tr>
                            <td>Roles</td>
                            <td>
                                {user?.roles.map((role)=>(role))}
                            </td>
                        </tr> */}
                    </tbody>
                </Table>
                {currentUser ? (
                    currentUser.id == user?.id ? (
                        <CardFooter className="text-center">
                            <Button color="warning">Update Profile</Button>
                        </CardFooter>
                    ) : (
                        ""
                    )
                ) : (
                    ""
                )}
            </CardBody>
        </Card>
    );
}

export default ViewUserProfile;
