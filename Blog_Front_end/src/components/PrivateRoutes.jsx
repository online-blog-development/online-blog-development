import React from 'react'
import { Navigate, Outlet } from 'react-router-dom'
import isLoggedIn from '../Auth/index'
const PrivateRoutes = () => {
    // From isLoggedIn method of auth
    if (isLoggedIn) {
        return <Outlet>

        </Outlet>
    } else
        return <Navigate to={"/login"}></Navigate>

}

export default PrivateRoutes