import React, { useState } from "react";
import { useEffect } from "react";
import { loadAllPosts } from "../services/post-service";
import {
  Row,
  Col,
  Pagination,
  PaginationItem,
  PaginationLink,
  Container,
} from "reactstrap";
import Post from "./Post";
import { toast } from "react-toastify";

function NewFeed() {
  const [postContent, setPostContent] = useState({
    content: [],
    totalPages: "",
    totalElements: "",
    pageSize: "",
    lastPage: false,
    pageNumber: "",
  });

  useEffect(() => {
    //load posts from server
    changePage(0);
  }, []);

  const changePage = (pageNumber = 0, pageSize = 5) => {
    if (pageNumber > postContent.pageNumber && postContent.lastPage) {
      return;
    }
    if (pageNumber < postContent.pageNumber && postContent.pageNumber == 0) {
      return;
    }
    loadAllPosts(pageNumber, pageSize)
      .then((data) => {
        setPostContent(data);
        console.log(data);
        window.scroll(0, 0);
      })
      .catch((error) => {
        toast.error("error in loading posts");
      });
  };

  return (
    <div className="container-fluid">
      <Row>
        <Col md={10}>
        <div>
            <h2
              style={{
                color: "navy",
                fontFamily: "Romantic",
                textAlign: "center",
              }}
            >
              Blog Feed
            </h2>
            <b
              style={{
                textAlign: "center",
                fontFamily: "Romantic",
                color: "black",
              }}
            >
              Blogs Count ({postContent?.totalElements})
            </b>
          </div>

          {postContent.content.map((post) => (
            <Post post={post} key={post.postId}></Post>
          ))}

          <Container>
            <Pagination>
              <PaginationItem
                onClick={() => changePage(postContent.pageNumber - 1)}
                disabled={postContent.pageNumber == 0}
              >
                <PaginationLink previous>Previous</PaginationLink>
              </PaginationItem>

              {[...Array(postContent.totalPages)].map((item, index) => (
                <PaginationItem
                  key={index}
                  active={index == postContent.pageNumber}
                  onClick={() => changePage(index)}
                >
                  <PaginationLink>{index + 1}</PaginationLink>
                </PaginationItem>
              ))}

              <PaginationItem
                onClick={() => changePage(postContent.pageNumber + 1)}
                disabled={postContent.lastPage}
              >
                <PaginationLink next>Next</PaginationLink>
              </PaginationItem>
            </Pagination>
          </Container>
        </Col>
      </Row>
    </div>
  );
}

export default NewFeed;
