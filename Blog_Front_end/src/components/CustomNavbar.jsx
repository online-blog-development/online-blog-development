import { NavLink as ReactLink, useNavigate } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
} from 'reactstrap';
import { isLoggedIn, doLogout, getUserName, getCurrentUserDetail, getRoles } from '../Auth';



const CustomNavbar = () => {
  let navigate = useNavigate()
  const [isOpen, setIsOpen] = useState(false);

  const [login, setLogin] = useState(false)
  const [user, setUser] = useState(undefined)
  const [userName, setUserName] = useState(undefined)
  const [roles, setRoles] = useState(undefined)

  useEffect(() => {
    setLogin(isLoggedIn())
    setUser(getCurrentUserDetail())
    setUserName(getUserName())
    setRoles(getRoles())

  }, [login])



  const toggle = () => setIsOpen(!isOpen);

  const logout = () => {
    doLogout(() => {
      setLogin(false)
      navigate("/")
    })
  }

  return (
    <div >
      <Navbar
        color="success"
        dark
        expand="md"
        // fixed="top"
        sticky='top'
        className="px-10 "
        
      >
        <NavbarBrand style={{fontSize: "35px", fontFamily: "Goudy Old Style"}}>The Daily Scroll</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="me-auto" navbar>
            <NavItem style={{fontSize: "20px", fontFamily: "BRADLY HAND ITC"}}>
              <NavLink tag={ReactLink} to="/" >Blogs</NavLink>
            </NavItem>

            {/* <NavItem>
              <NavLink tag={ReactLink} to="/about" >About</NavLink>
            </NavItem> */}

            {/* <NavItem>
              <NavLink tag={ReactLink} to="/services" >Services</NavLink>
            </NavItem> */}

            <NavItem style={{fontSize: "20px", fontFamily: "BRADLY HAND ITC"}}>
              <NavLink tag={ReactLink} to="/user/dashboard">Create Post</NavLink>
            </NavItem>


            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret style={{fontSize: "20px", fontFamily: "BRADLY HAND ITC"}}>
                More
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem tag={ReactLink} >Contact Us</DropdownItem>
                <DropdownItem>Facebook</DropdownItem>
                <DropdownItem divider />
                <DropdownItem>Instagram</DropdownItem>
                <DropdownItem>LinkedIn</DropdownItem>

              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
          <Nav navbar>

            {
              login && (
                <>
                {
                  (roles[0].id == 501) ?
                (<NavItem style={{fontSize: "20px", fontFamily: "BRADLY HAND ITC"}}>
                  <NavLink tag={ReactLink} to={`/user/admin/`}>
                  Admin
                  </NavLink>
                </NavItem>) : ("")
                }

                  <NavItem style={{fontSize: "20px", fontFamily: "BRADLY HAND ITC"}}>
                    <NavLink tag={ReactLink} to={`/user/profile-info/${user}`}>
                      Profile-Info
                    </NavLink>
                  </NavItem>

                  <NavItem style={{fontSize: "20px", fontFamily: "BRADLY HAND ITC"}}>
                    <NavLink tag={ReactLink} to="/user/dashboard">
                    {userName}
                    </NavLink>
                  </NavItem >
                  <NavItem style={{fontSize: "20px", fontFamily: "BRADLY HAND ITC"}}>
                    <NavLink onClick={logout}>
                      Logout
                    </NavLink>
                  </NavItem>

                </>
              )
            }

            {

              !login && (
                <>
                  <NavItem>
                    <NavLink tag={ReactLink} to="/login">
                      Login
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink tag={ReactLink} to="/signup">
                      Signup
                    </NavLink>
                  </NavItem>
                </>
              )
            }

          </Nav>


        </Collapse>
      </Navbar>
    </div>
  );
}

export default CustomNavbar;
