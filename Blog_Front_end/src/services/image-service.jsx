import { privateAxios } from "./helper";

export const uploadImage=(image, postId)=>{
    let formData=new FormData()
    formData.append("image", image);

    return privateAxios
    .post(`/post/image/upload/${postId}`, formData,{
        headers:{
            'Content-Type':'multipart/form-data'
        }
    })
    // .then((response)=>response.data)
}