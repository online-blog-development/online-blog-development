import { privateAxios } from "./helper"

export const createComment =(comment, uid, postId)=>{
    // debugger;
    return privateAxios.post(`/comments/users/${uid}/post/${postId}/comments`, comment)
    // .then(response=>response.content)
}