import { privateAxios } from "./helper";
import { myAxios } from "./helper";
//Create Post
export const createPost = (postData) => {
    console.log(postData)
    // debugger
    return privateAxios
        .post(`/user/${postData.userId}/category/${postData.categoryId}/posts`, postData)
        // .then((response) => response.postData);
};

//load all posts
export const loadAllPosts = (pageNumber,pageSize)=>{

    return myAxios
    .get(`/posts?pageNumber=${pageNumber}&pageSize=${pageSize}`)
    .then(response=>response.data)
}

//load post from postId
export const loadPost=(postId)=>{
    return myAxios
    .get(`/posts/`+postId)
    .then(response=>response.data)
}

export const deletePostService=(postId)=>{
    return privateAxios
    .delete(`/posts/${postId}`)
    .then((response)=>response.data)
}
export const loadPostUserWise=(userId)=>{
    return privateAxios
    .get(`/user/${userId}/posts`)
    .then((response)=>response.data)
}