import axios from "axios";
import { getToken } from "../Auth";

export const BASE_URL = "http://localhost:8080/blog";

export const myAxios = axios.create({
  baseURL: BASE_URL,
  // headers: {
  //     'Content-Type': 'application/json',
  //     // 'Access-Control-Allow-Origin': '*',
  //     // 'Access-Control-Allow-Credentials': 'true'
  // }
});

export const privateAxios = axios.create({
  baseURL: BASE_URL,
});

privateAxios.interceptors.request.use(
  (config) => {
    const token = getToken();
    console.log(token);
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
      return config;
    }
  },
  (error) => Promise.reject(error)
);
