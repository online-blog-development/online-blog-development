import { myAxios, privateAxios } from './helper'

//get all categories
export const loadAllCategories = () => {
    return myAxios
        .get('/categories/')
        .then(response => { return response.data })
}

export const loadPostCategoryWise=(categoryId)=>{

    return privateAxios
    .get(`/category/${categoryId}/posts`)
    .then((response)=>response.data)
}

