

//isLoggedIn=>
export const isLoggedIn = () => {
  // debugger;
  var data = localStorage.getItem("data");
  // console.log(data);
  if (data == null) {
    return false;
  } else {
    return true;
  }
};

//doLogIn=>data=>set to local storage
export const doLogin = (data, next) => {
  localStorage.setItem("data", JSON.stringify(data));
  next();
};

//doLogout=> remove from locatStorage
export const doLogout = (next) => {
  localStorage.removeItem("data");
  next();
};

//get current user
export const getCurrentUserDetail = () => {
  // debugger;
  if (isLoggedIn()) {
    return JSON.parse(localStorage.getItem("data")).userId;

  } else {
    return undefined;
  }
};

export const getToken = () => {
  if (isLoggedIn()) {
    return JSON.parse(localStorage.getItem('data')).token
  } else {
    return null;
  }
};

export const getUserName = () => {
  if (isLoggedIn()) {
    return JSON.parse(localStorage.getItem('data')).uname
  } else {
    return undefined;
  }
};

export const getRoles = () => {
  if (isLoggedIn()) {
    // debugger
    return JSON.parse(localStorage.getItem('data')).roles;

  }
  else {
    return undefined;
  }
}




export default isLoggedIn;

