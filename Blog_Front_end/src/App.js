import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'reactstrap';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import Login from './pages/Login';
import Signup from './pages/SignUp';
import Services from './pages/Services';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import UserDashboard from './pages/user-routes/UserDashboard';
import PrivateRoutes from './components/PrivateRoutes';
import ProfileInfo from './pages/user-routes/ProfileInfo';
import PostPage from './pages/PostPage';
import Categories from './pages/Categories';
import Dashboard from './pages/user-routes/Admin/dashboard'
import Users from './pages/user-routes/Admin/users';
import Category from './pages/user-routes/Admin/category';


function App() {
  return (


    <BrowserRouter>
      <ToastContainer position='bottom-center' />
      <Routes>

        <Route path="/" element={<Home></Home>} />
        <Route path="/home" element={<Home></Home>} />
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<Signup></Signup>} />
        <Route path="/services" element={<Services></Services>} />
        <Route path='/posts/:postId' element={<PostPage></PostPage>}></Route>
        <Route path='/categories/:categoryId' element={<Categories></Categories>}></Route>

        <Route path='/user' element={<PrivateRoutes></PrivateRoutes>}>
          <Route path='dashboard' element={<UserDashboard></UserDashboard>}></Route>
          <Route path='profile-info/:userId' element={<ProfileInfo></ProfileInfo>}></Route>
          <Route path='admin' element={<Dashboard></Dashboard>}></Route>
          <Route path='admin/users' element={<Users></Users>}></Route>
          <Route path='admin/category' element={<Category></Category>}></Route>
          
        </Route>



      </Routes>
    </BrowserRouter>


  );
}

export default App;
